package com.financeactive;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.financeactive.model.Product;
import com.financeactive.model.ProductCategories;
import com.financeactive.service.InvoiceService;
import com.financeactive.service.impl.InvoiceServiceImpl;
import com.financeactive.utils.Utils;

public class App {
	public static void main(String[] args) {
		System.out.println((BigDecimal.valueOf(5).add(BigDecimal.valueOf(0.5)).multiply(BigDecimal.TEN)));
	
		InvoiceService invoiceService = new InvoiceServiceImpl();
		// calcul taxe input 1
		invoiceService.printInvoice(input1());
		System.out.println("");

		// calcul taxe input 2
		invoiceService.printInvoice(input2());
		System.out.println("");
		// calcul taxe input 3
		invoiceService.printInvoice(input3());
	}

	private static List<Product> input1() {
		return Arrays.asList(new Product("livre", ProductCategories.BOOKS, Utils.doubleToBD(12.49), false, 1),
				new Product("CD musical", ProductCategories.OTHER, Utils.doubleToBD(14.99), false, 1),
				new Product("barre de chocolat", ProductCategories.FOOD, Utils.doubleToBD(0.85), false, 1));
	}

	private static List<Product> input2() {
		return Arrays.asList(
				new Product("boîte de chocolats importée", ProductCategories.FOOD, Utils.doubleToBD(10.00), true, 1),
				new Product("flacon de parfum importé", ProductCategories.OTHER, Utils.doubleToBD(47.50), true, 1));
	}

	private static List<Product> input3() {
		return Arrays.asList(
				new Product("flacon de parfum importé", ProductCategories.OTHER, Utils.doubleToBD(27.99), true, 2),
				new Product("flacon de parfum", ProductCategories.OTHER, Utils.doubleToBD(18.99), false, 1),
				new Product("boîte de pilules contre la migraine", ProductCategories.DRUGS, Utils.doubleToBD(9.75),
						false, 1),
				new Product("boîte de chocolats importés", ProductCategories.FOOD, Utils.doubleToBD(11.25), true, 1));
	}

}
