package com.financeactive.model;

import java.math.BigDecimal;

public class Product {

	private String name;

	private ProductCategories category;

	private BigDecimal price;

	private boolean imported;

	private int quantity;

	public Product() {
	}

	public Product(String name, ProductCategories category, BigDecimal price, boolean imported, int quantity) {
		super();
		this.name = name;
		this.category = category;
		this.price = price;
		this.imported = imported;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductCategories getCategory() {
		return category;
	}

	public void setCategory(ProductCategories category) {
		this.category = category;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", category=" + category + ", price=" + price + ", imported=" + imported
				+ ", quantity=" + quantity + "]";
	}

}
