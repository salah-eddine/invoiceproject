package com.financeactive.model;

public enum ProductCategories {
	BOOKS, FOOD, DRUGS, OTHER
}
