package com.financeactive.service;

import java.util.List;

import com.financeactive.model.Product;

public interface InvoiceService {
	void printInvoice(List<Product> products);
}
