package com.financeactive.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.financeactive.model.Product;
import com.financeactive.model.ProductCategories;
import com.financeactive.service.InvoiceService;
import com.financeactive.utils.Utils;

public class InvoiceServiceImpl implements InvoiceService {

	public void printInvoice(List<Product> products) {
		BigDecimal taxes = Utils.doubleToBD(0);
		BigDecimal total = Utils.doubleToBD(0);
		BigDecimal priceTTC;
		BigDecimal singleProductTaxeValue;
		for (Product product : products) {
			singleProductTaxeValue = computeTaxe(product);
			priceTTC = product.getPrice().add(singleProductTaxeValue);
			System.out.println(product.getQuantity() + " " + product.getName() + " : "
					+ priceTTC.multiply(new BigDecimal(product.getQuantity())));
			taxes = (taxes.add(singleProductTaxeValue)).multiply(new BigDecimal(product.getQuantity()));
			total = (total.add(priceTTC)).multiply(new BigDecimal(product.getQuantity()));
		}
		System.out.println("Montant des taxes : " + taxes);
		System.out.println("Total : " + total);
	}

	private BigDecimal computeTaxe(Product product) {
		BigDecimal taxe = Utils.doubleToBD(0);
		if (!checkIfProductIsExemptFromTaxe(product)) {
			taxe = taxe.add(Utils.roundedPrice(product.getPrice().floatValue() * 10 / 100));
		}
		if (product.isImported()) {
			taxe = taxe.add(Utils.roundedPrice(product.getPrice().floatValue() * 5 / 100));
		}
		return taxe;
	}

	private boolean checkIfProductIsExemptFromTaxe(Product product) {
		return ProductCategories.BOOKS.equals(product.getCategory())
				|| ProductCategories.FOOD.equals(product.getCategory())
				|| ProductCategories.DRUGS.equals(product.getCategory());
	}

}
