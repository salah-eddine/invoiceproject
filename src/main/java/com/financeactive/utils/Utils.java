package com.financeactive.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

	public static BigDecimal doubleToBD(double val) {
		return BigDecimal.valueOf(val).setScale(2);
	}

	public static BigDecimal roundedPrice(double price) {
		return roundToSuperiorUp(BigDecimal.valueOf(price), BigDecimal.valueOf(0.05));
	}

	public static BigDecimal roundToSuperiorUp(BigDecimal value, BigDecimal rounding) {
		return round(value, rounding, RoundingMode.CEILING);

	}

	public static BigDecimal round(BigDecimal value, BigDecimal rounding, RoundingMode roundingMode) {
		return rounding.signum() == 0 ? value : (value.divide(rounding, 0, roundingMode)).multiply(rounding);

	}
}
