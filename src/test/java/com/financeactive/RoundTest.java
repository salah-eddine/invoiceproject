package com.financeactive;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.financeactive.utils.Utils;

public class RoundTest {

	@Test
	public void test1() {
		assertEquals(Utils.doubleToBD(1.0), Utils.roundedPrice(0.99));
	}

	@Test
	public void test2() {
		assertEquals(Utils.doubleToBD(1.0), Utils.roundedPrice(1.0));
	}

	@Test
	public void test3() {
		assertEquals(Utils.doubleToBD(1.05), Utils.roundedPrice(1.01));
	}

	@Test
	public void test4() {
		assertEquals(Utils.doubleToBD(1.05), Utils.roundedPrice(1.02));
	}
}
